extends LinkButton

export(String) var scene_to_load

# Play Button
func _on_LinkButton_pressed():
	global.ms = 0
	global.s = 0
	global.m = 0
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
