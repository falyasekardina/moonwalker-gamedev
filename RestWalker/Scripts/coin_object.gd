extends Node2D

export var value = 1
	
func _on_collect_body_entered(body):
	if body.get_name() == "Player":
		if global.coins >= 0:
			global.coins += value	
			global.yellow_collected += 1
			queue_free()

