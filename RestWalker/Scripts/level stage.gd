extends Node2D

func _on_Level_2_pressed():
	if global.coins >= 40:
		get_tree().change_scene(str("res://Scenes/WinterScene.tscn"))
	else:
		print(global.coins)
		var infoPopupMenu = get_node("PopupMenu")
		infoPopupMenu.show()


func _on_Level_1_pressed():
	global.ms = 0
	global.s = 0
	global.m = 0
	get_tree().change_scene(str("res://Scenes/ForestScene.tscn"))


func _on_back_pressed():
		get_tree().change_scene(str("res://Scenes/MainScene.tscn"))
