extends Node2D

export var value = 5

func _on_coinred_body_entered(body):
	if body.get_name() == "Player":
		if global.coins >= 0:
			global.coins += value
			global.red_collected += 1
			queue_free()
	
