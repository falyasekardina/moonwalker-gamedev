extends Area2D

export (String) var sceneName = "WinterScene"

func _on_lose_body_entered(body):
	if body.get_name() == "Player":
		global.coins = global.coins_level1
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
