extends Area2D

export (String) var sceneName = "ForestScene"

func _on_Coin_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
		if current_scene == sceneName:
			global.coins +=1
		if (global.coins == 0):
			pass
		else:
			get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
