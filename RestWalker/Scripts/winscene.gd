extends Node2D

export (String) var sceneName = "WinterScene"

func _on_win_body_entered(body):
	if body.get_name() == "Player":
		get_parent().get_node("CanvasLayer/TimerGUI/Text Displayer/ms").stop()
		global.coins_level2 = global.coins
		var popupMenu = get_node("PopupMenu")
		popupMenu.show()
		
		
