extends Area2D

export (String) var sceneName = "ForestScene"

func _on_Lose_body_entered(body):
	if body.get_name() == "Player":
		global.coins = 0
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
	

func _on_Win_body_entered(body):
	if body.get_name() == "Player":
		global.coins_level1 = global.coins
		get_tree().change_scene(str("res://Scenes/level stage.tscn"))
